const express = require("express");
const app = express();
const morgan = require("morgan");
const path = require("path");
const db = require("./models/index");

const port = process.env.PORT || 3000;

app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("register");
});

//Routes
const userRoutes = require("./routes/user.route");
const biodataRoutes = require("./routes/biodata.route");
const matchRoutes = require("./routes/match.route");

app.use("/user", userRoutes);
app.use("/biodatas", biodataRoutes);
// app.use("/match", matchRoutes);

app.use((req, res, next) => {
  const error = new Error("Not Found!");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

app.listen(port, () => {
  console.log(`Server ada di port ${port}`);
});
