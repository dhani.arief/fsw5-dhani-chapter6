"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class BiodataUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BiodataUser.belongsTo(models.User, {
        foreignKey: {
          name: "user_id",
          as: "user",
        },
      });
    }
  }
  BiodataUser.init(
    {
      user_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      level: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "BiodataUser",
    }
  );
  return BiodataUser;
};
