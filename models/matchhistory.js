"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class MatchHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      MatchHistory.belongsTo(models.User, {
        foreignKey: {
          name: "user_id",
          as: "user",
        },
      });
    }
  }
  MatchHistory.init(
    {
      user_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      win: DataTypes.INTEGER,
      lose: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "MatchHistory",
    }
  );
  return MatchHistory;
};
