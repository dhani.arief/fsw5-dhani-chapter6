const express = require("express");
const router = express.Router();
const db = require("../models/index");
// const User = require('../models/user')

router.get("/", (req, res) => {
  db.User.findAll()
    .then((users) => {
      res.status(200).json(users);
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

router.get("/:id", (req, res) => {
  db.User.findByPk(req.params.id)
    .then((user) => {
      res.status(200).json(user);
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

router.put("/:id", (req, res) => {
  db.User.update(req, body)
    .then((user) => {
      res.redirect("/user");
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

router.delete("/:id", (req, res) => {
  db.User.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((user) => {
      res.redirect("/user");
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

router.post("/", (req, res) => {
  const { username, password, name, email } = req.body;
  db.User.create({
    username,
    password,
    name,
    email,
    createdAt: new Date(),
    updatedAt: new Date(),
  })
    .then((user) => {
      res.redirect("/");
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

module.exports = router;
