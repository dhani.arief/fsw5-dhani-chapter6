const express = require("express");
const router = express.Router();
const db = require("../models/index");
// const User = require('../models/user')

router.get("/", (req, res) => {
  db.BiodataUser.findAll({})
    .then((biodatausers) => {
      res.status(200).json(biodatausers);
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

router.get("/:id", (req, res) => {
  db.BiodataUser.findByPk(req.params.id)
    .then((biodata) => {
      res.status(200).json(biodata);
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

router.post("/", (req, res) => {
  const { user_id } = req.body;

  db.BiodataUser.create({
    user_id,
  })
    .then((biodatauser) => {
      res.status(200).json(biodatauser);
    })
    .catch((e) => {
      res.status(400).json(e.message);
    });
});

module.exports = router;
